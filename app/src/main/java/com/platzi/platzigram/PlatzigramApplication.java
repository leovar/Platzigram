package com.platzi.platzigram;

import android.app.Application;

/**
 * Created by leonardo.valencia on 20/04/2018.
 */

/*En esta clase colocaremos tolo lo que tenga que ver con Firebase*/
/*para que las variables de FireBase permanezcan durante toda la vida de la app*/
public class PlatzigramApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
    }
}
