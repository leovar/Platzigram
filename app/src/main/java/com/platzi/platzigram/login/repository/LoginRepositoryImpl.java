package com.platzi.platzigram.login.repository;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.login.interactor.LoginInteractor;
import com.platzi.platzigram.login.presenter.LoginPresenter;
import com.platzi.platzigram.login.view.LoginActivity;

/**
 * Created by leonardo.valencia on 26/02/2018.
 */

public class LoginRepositoryImpl implements LoginRepository {

    private static final String TAG = "LoginRepositoryImpl";
    FirebaseAuth firebaseAuth;
    FirebaseAuth.AuthStateListener authStateListener;

    LoginInteractor interactor;

    public LoginRepositoryImpl(LoginInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void SingIn(String user, String password, LoginActivity loginActivity, FirebaseAuth firebaseAuth) {

        firebaseAuth.signInWithEmailAndPassword(user, password).addOnCompleteListener(loginActivity, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                interactor.loginSuccess(task);
            }
        });
    }
}
