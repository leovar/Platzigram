package com.platzi.platzigram.login.view;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.platzi.platzigram.CreateAccount.view.CreateAccountActivity;
import com.platzi.platzigram.R;
import com.platzi.platzigram.login.presenter.LoginPresenter;
import com.platzi.platzigram.login.presenter.LoginPresenterImpl;
import com.platzi.platzigram.views.ContainerActivity;

public class LoginActivity extends AppCompatActivity implements loginView {

    private static final String TAG = "CreateAccountActivity";
    FirebaseAuth firebaseAuth;
    FirebaseAuth.AuthStateListener authStateListener;

    private TextInputEditText username, passwowrd;
    private Button login;
    private TextView sinCuenta, crear_cuenta;
    private ProgressBar loginProgressBar;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //TODO tratar de validar estos datos desde el Repository
        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.w(TAG, "Usuario Logueado " + user.getEmail());
                } else {
                    Log.w(TAG, "Usuario no Logueado");
                }
            }
        };

        username = (TextInputEditText) findViewById(R.id.username);
        passwowrd = (TextInputEditText) findViewById(R.id.passwowrd);
        login = (Button) findViewById(R.id.login);
        sinCuenta = (TextView) findViewById(R.id.sinCuenta);
        crear_cuenta = (TextView) findViewById(R.id.crear_cuenta);
        loginProgressBar = (ProgressBar) findViewById(R.id.loginProgressBar);
        presenter = new LoginPresenterImpl(this);
        ocultarProgressBar();
        mostrarControles();
    }

    @Override
    public void mostrarControles() {
        username.setEnabled(true);
        passwowrd.setEnabled(true);
        login.setEnabled(true);
        sinCuenta.setEnabled(true);
        crear_cuenta.setEnabled(true);
    }

    @Override
    public void ocultarControles() {
        username.setEnabled(false);
        passwowrd.setEnabled(false);
        login.setEnabled(false);
        sinCuenta.setEnabled(false);
        crear_cuenta.setEnabled(false);
    }

    @Override
    public void mostraProgressBar() {
        loginProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void ocultarProgressBar() {
        loginProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void CrearCuentaLogin(View view) {
        Intent intentCuenta = new Intent(this, CreateAccountActivity.class);
        startActivity(intentCuenta);
    }

    @Override
    public void ButtonLoginClick(View view) {
        if (username.getText().toString().equals("")){
            loginShowError("Usuario Incorrecto");
            return;
        }
        if (passwowrd.getText().toString().equals("")){
            loginShowError("Password Incorrecto");
            return;
        }

        String user = username.getText().toString();
        String pass = passwowrd.getText().toString();
        presenter.Login(user, pass, this, firebaseAuth);
    }

    @Override
    public void linkButtonClickPlatzi(View view) {
        //intent inplicito donde se direcciona a un browser para abrir una direccion url
        Intent intentPlatzi = new Intent(Intent.ACTION_VIEW);
        intentPlatzi.setData(Uri.parse("http://www.platzi.com"));
        startActivity(intentPlatzi);
    }

    @Override
    public void goHome() {
        Intent intentLogin = new Intent(this, ContainerActivity.class);
        startActivity(intentLogin);
    }

    @Override
    public void loginShowError(String error) {
        CharSequence mensaje = error;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(this, mensaje, duration);
        toast.show();

        // Otra manera de hacer Toast es
        // Toast.makeText(this, getString("Ocurrio un Error"), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(authStateListener);
    }
}
