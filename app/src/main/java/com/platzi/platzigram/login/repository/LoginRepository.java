package com.platzi.platzigram.login.repository;

import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.login.view.LoginActivity;

/**
 * Created by leonardo.valencia on 26/02/2018.
 */

public interface LoginRepository {

    void SingIn(String user, String password, LoginActivity loginActivity, FirebaseAuth firebaseAuth);
}
