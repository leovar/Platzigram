package com.platzi.platzigram.login.interactor;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.login.presenter.LoginPresenter;
import com.platzi.platzigram.login.repository.LoginRepository;
import com.platzi.platzigram.login.repository.LoginRepositoryImpl;
import com.platzi.platzigram.login.view.LoginActivity;

/**
 * Created by leonardo.valencia on 26/02/2018.
 */

public class LoginInteractorImpl implements LoginInteractor {

    private LoginPresenter presenter;
    private LoginRepository repository;

    public LoginInteractorImpl(LoginPresenter presenter) {
        this.presenter = presenter;
        repository = new LoginRepositoryImpl(this);
    }

    @Override
    public void SingIn(String user, String password, LoginActivity loginActivity, FirebaseAuth firebaseAuth) {
        repository.SingIn(user, password, loginActivity, firebaseAuth);
    }

    @Override
    public void loginSuccess(Task<AuthResult> taskResult) {
        if (taskResult.isSuccessful()){
            presenter.loginSuccess();
        }else{
            Exception ex = taskResult.getException();
            presenter.loginShowError("error " + ex.getMessage());
        }
    }

    @Override
    public void loginShowError(String result) {

    }
}
