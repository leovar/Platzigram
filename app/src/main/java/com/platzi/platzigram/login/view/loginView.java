package com.platzi.platzigram.login.view;

import android.view.View;

/**
 * Created by leonardo.valencia on 29/01/2018.
 */

public interface loginView {

    void mostrarControles();
    void ocultarControles();

    void mostraProgressBar();
    void ocultarProgressBar();

    void CrearCuentaLogin(View view);
    void ButtonLoginClick(View view);
    void linkButtonClickPlatzi(View view);
    void goHome();

    void loginShowError(String error);

}
