package com.platzi.platzigram.login.presenter;

import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.login.view.LoginActivity;

/**
 * Created by leonardo.valencia on 26/02/2018.
 */

public interface LoginPresenter {

    void Login(String user, String password, LoginActivity loginActivity, FirebaseAuth firebaseAuth);   //Interactor
    void loginSuccess();        //View
    void loginShowError(String result); //View
}
