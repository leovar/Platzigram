package com.platzi.platzigram.login.interactor;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.login.view.LoginActivity;

/**
 * Created by leonardo.valencia on 26/02/2018.
 */

public interface LoginInteractor {

    void SingIn(String user, String password, LoginActivity loginActivity, FirebaseAuth firebaseAuth);

    void loginSuccess (Task<AuthResult> tastResult);

    void loginShowError(String result);

}
