package com.platzi.platzigram.login.presenter;

import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.login.interactor.LoginInteractor;
import com.platzi.platzigram.login.interactor.LoginInteractorImpl;
import com.platzi.platzigram.login.view.LoginActivity;

/**
 * Created by leonardo.valencia on 26/02/2018.
 */

public class LoginPresenterImpl implements LoginPresenter {

    private LoginActivity loginView;
    private LoginInteractor interactor;

    public LoginPresenterImpl(LoginActivity loginActivity) {
        this.loginView = loginActivity;
        interactor = new LoginInteractorImpl(this);
    }

    @Override
    public void Login(String user, String password, LoginActivity loginActivity, FirebaseAuth firebaseAuth) {
        loginView.ocultarControles();
        loginView.mostraProgressBar();
        interactor.SingIn(user, password, loginActivity, firebaseAuth);
    }

    @Override
    public void loginSuccess() {
        loginView.goHome();
        loginView.ocultarProgressBar();
        loginView.mostrarControles();
    }

    @Override
    public void loginShowError(String result) {
        loginView.ocultarProgressBar();
        loginView.mostrarControles();
        loginView.loginShowError(result);
    }
}
