package com.platzi.platzigram.postNewImage.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.platzi.platzigram.R;

import java.io.File;

public class NewPostActivity extends AppCompatActivity {

    private ImageView imgPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        //Obtengo la Ruta que le estoy enviando como parametro por medio del intent
        imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
        if (getIntent().getExtras() != null){
            String photoPath = getIntent().getExtras().getString("PHOTO_PATH_TEMP");
            File f = new File(photoPath); //("/storage/3326-191B/Canon EOS 1300D/IMG_20180329_190027.jpg");
            if (f.exists())
            {
                Bitmap bMap = BitmapFactory.decodeFile(f.getAbsolutePath());
                Bitmap resized = Bitmap.createScaledBitmap(bMap,(int)(bMap.getWidth()*0.8), (int)(bMap.getHeight()*0.8), true);
                imgPhoto.setImageBitmap(resized);
                //Picasso.with(this).load(f).into(resized);
            }
        }

    }
}
