package com.platzi.platzigram.postNewImage.view;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.platzi.platzigram.R;
import com.platzi.platzigram.adapter.PictureAdapterRecyclerView;
import com.platzi.platzigram.model.Picture;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private static final int REQUEST_CAMERA = 1;
    private FloatingActionButton fabCamera;
    private String photoPathTemp = "";
    public static String PACKAGE_NAME;
    public Activity _Activity = null;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //Obtengo el dato enviado desde el activiti que inicia el fragment
        String id = getArguments() != null ? getArguments().getString("id") : "myId";

        _Activity = getActivity();

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ShowToolbar(getResources().getString(R.string.tab_home), false, view);
        RecyclerView picturesRecycler = (RecyclerView) view.findViewById(R.id.pictureRecycler);
        fabCamera = (FloatingActionButton) view.findViewById(R.id.fabCamera);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        picturesRecycler.setLayoutManager(linearLayoutManager);

        PictureAdapterRecyclerView pictureAdapterRecyclerView = new PictureAdapterRecyclerView(buildPictures(), R.layout.cardview_picture, getActivity());
        picturesRecycler.setAdapter(pictureAdapterRecyclerView);

        fabCamera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        //Obtenemos el nombre del package de la aplicación para usarlo al crear el uri de la foto
        PACKAGE_NAME = getContext().getPackageName();

        return view;
    }

    private void takePicture() {

        //Inicializo el Intent de la camara
        Intent intenTakePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        //Valido que el celular si tenga camara
        if (intenTakePicture.resolveActivity(getActivity().getPackageManager()) != null){
            // Primero creamos un archivo en blanco en la carpeta temporal, antes de abrir la camara para tomar la foto
            // es como hacer un molde donde al tomar la foto se va a guardar la imagen

            File photoFile = null;

            try {
                photoFile = createImageFile();
            }
            catch(Exception e){
                //e.printStackTrace();
                Log.d("ERROR", e.getMessage());
            }

            if (photoFile != null && photoFile.exists())
            {
                // Creamos el contenedor a la camara
                Uri photoUri = FileProvider.getUriForFile(_Activity, PACKAGE_NAME, photoFile);

                // Agrego el Contenedor al Intent para que sepa donde me va a retornar la imagen
                // EN el starActivity agrego un codigo que me va a identificar la actividad de la camara
                intenTakePicture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(intenTakePicture, REQUEST_CAMERA);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HH-mm-ss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File photo = File.createTempFile(imageFileName, ".jpg", storageDir);
        String val = "";
        if (photo.exists())
        {
            val = photo.getAbsolutePath();
        }
        File fileReturn = new File(photo.getAbsolutePath());
        photoPathTemp = val;//fileReturn.getAbsolutePath().toString();
        return photo;
    }

    //Metodo para obtener el nombre de la aplicacion; no usado en esta aplicacion aun
    public String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    // Este metodo se sobrescribe y se invoca cuando una actividad habierta desde esta devuelve algo
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Valido que el requestCode que me retorna sea de la actividad de la camara
        if (requestCode == REQUEST_CAMERA && resultCode == getActivity().RESULT_OK){
            Log.i("HomeFragment", "CAMERA OK!!");
            Intent i = new Intent(getActivity(), NewPostActivity.class);
            i.putExtra("PHOTO_PATH_TEMP", photoPathTemp);
            startActivity(i);
        }
    }

    //Clase para armar el Array de imagenes con sus caracteristicas
    public ArrayList<Picture> buildPictures(){
        ArrayList<Picture> pictures = new ArrayList<>();
        pictures.add(new Picture("http://www.viajeatailandia.com/wp-content/uploads/2016/08/Krabi.jpg", "Sandra Alzate", "10 dias", "6 Me Gusta"));
        pictures.add(new Picture("http://www.viajeatailandia.com/wp-content/uploads/2017/08/Playa-de-Bang-Tao.jpg", "Juan Pablo", "8 dias", "5 Me Gusta"));
        pictures.add(new Picture("http://www.magictours.com/wp-content/uploads/2017/05/ORLANDO.jpg", "Jaime y Dolli", "15 dias", "4 Me Gusta"));
        pictures.add(new Picture("https://sumedico.com/wp-content/uploads/2017/11/reproducci%C3%B3n_de_los_delfines.jpg", "Familia", "9 dias", "10 Me Gusta"));
        return pictures;
    }

    public void ShowToolbar(String tittle, boolean upButton, View view)
    {
        //Para el caso de un fragment como este, se usa el view que es donde esta cargado el layaut actual
        //del fragment, y dentro de este view es donde vamos a buscar la toolbar
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        //como trabajamos con soporte debemos aplicar la siguiente linea
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(tittle);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }
}
