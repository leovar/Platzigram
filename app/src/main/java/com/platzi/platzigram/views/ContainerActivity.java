package com.platzi.platzigram.views;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.platzi.platzigram.R;
import com.platzi.platzigram.postNewImage.view.HomeFragment;
import com.platzi.platzigram.views.fragment.ProfileFragment;
import com.platzi.platzigram.views.fragment.SearchFragment;

public class ContainerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        //Instancio el BottonNavigation y genero el listener para estar pendiente de lo que se presione
        BottomNavigationView buttonBar = (BottomNavigationView) findViewById(R.id.bottombar);


        buttonBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //El Bundle se usa para instanciar en envio de datos entre fragments
                Bundle argumentsSend = new Bundle();
                switch (item.getItemId()){
                    case R.id.itemHome:
                        argumentsSend.putString("id", "paramHome");
                        HomeFragment homeFragment = new HomeFragment();
                        homeFragment.setArguments(argumentsSend);
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, homeFragment)
                                .setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .addToBackStack(null)
                                .commit();
                        break;
                    case R.id.itemProfile:
                        argumentsSend.putString("id", "paramPerfil");
                        ProfileFragment profileFragment = new ProfileFragment();
                        profileFragment.setArguments(argumentsSend);
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, profileFragment)
                                .setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .addToBackStack(null)
                                .commit();
                        break;
                    case R.id.itemSearch:
                        argumentsSend.putString("id", "paramSearch");
                        SearchFragment searchFragment = new SearchFragment();
                        searchFragment.setArguments(argumentsSend);
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, searchFragment)
                                .setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .addToBackStack(null)
                                .commit();
                        break;
                }
                return true;
            }
        });

        buttonBar.setSelectedItemId(R.id.itemHome);
    }
}
