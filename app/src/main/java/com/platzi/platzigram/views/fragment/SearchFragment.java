package com.platzi.platzigram.views.fragment;


import android.app.SearchManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.platzi.platzigram.R;
import com.platzi.platzigram.adapter.PictureAdapterRecyclerView;
import com.platzi.platzigram.model.Picture;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {


    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        // Inflate the layout for this fragment

        String id = getArguments() != null ? getArguments().getString("id") : "myId";
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        RecyclerView picturesSearchRecycler = (RecyclerView) view.findViewById(R.id.pictureSearchRecycler);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        picturesSearchRecycler.setLayoutManager(gridLayoutManager);

        PictureAdapterRecyclerView pictureAdapterRecyclerView = new PictureAdapterRecyclerView(buildPictures(), R.layout.cardview_picture, getActivity());
        picturesSearchRecycler.setAdapter(pictureAdapterRecyclerView);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menuSearch);
        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setQueryHint("Search");

        super.onCreateOptionsMenu(menu, inflater);
    }

    public ArrayList<Picture> buildPictures(){
        ArrayList<Picture> pictures = new ArrayList<>();
        pictures.add(new Picture("http://www.viajeatailandia.com/wp-content/uploads/2016/08/Krabi.jpg", "Sandra Alzate", "10 dias", "6 Me Gusta"));
        pictures.add(new Picture("http://www.viajeatailandia.com/wp-content/uploads/2017/08/Playa-de-Bang-Tao.jpg", "Juan Pablo", "8 dias", "5 Me Gusta"));
        pictures.add(new Picture("http://www.magictours.com/wp-content/uploads/2017/05/ORLANDO.jpg", "Jaime y Dolli", "15 dias", "4 Me Gusta"));
        pictures.add(new Picture("https://sumedico.com/wp-content/uploads/2017/11/reproducci%C3%B3n_de_los_delfines.jpg", "Familia", "9 dias", "10 Me Gusta"));
        return pictures;
    }
}
