package com.platzi.platzigram.views.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.platzi.platzigram.R;
import com.platzi.platzigram.adapter.PictureAdapterRecyclerView;
import com.platzi.platzigram.model.Picture;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        String id = getArguments() != null ? getArguments().getString("id") : "myId";

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ShowToolbar("", false, view);

        RecyclerView picturesRecycler = (RecyclerView) view.findViewById(R.id.pictureProfileRecycler);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        picturesRecycler.setLayoutManager(linearLayoutManager);

        PictureAdapterRecyclerView pictureAdapterRecyclerView = new PictureAdapterRecyclerView(buildPictures(), R.layout.cardview_picture, getActivity());

        picturesRecycler.setAdapter(pictureAdapterRecyclerView);

        return view;
    }

    public ArrayList<Picture> buildPictures(){
        ArrayList<Picture> pictures = new ArrayList<>();
        pictures.add(new Picture("http://www.viajeatailandia.com/wp-content/uploads/2016/08/Krabi.jpg", "Sandra Alzate", "10 dias", "6 Me Gusta"));
        pictures.add(new Picture("http://www.viajeatailandia.com/wp-content/uploads/2017/08/Playa-de-Bang-Tao.jpg", "Juan Pablo", "8 dias", "5 Me Gusta"));
        pictures.add(new Picture("http://www.magictours.com/wp-content/uploads/2017/05/ORLANDO.jpg", "Jaime y Dolli", "15 dias", "4 Me Gusta"));
        pictures.add(new Picture("https://sumedico.com/wp-content/uploads/2017/11/reproducci%C3%B3n_de_los_delfines.jpg", "Familia", "9 dias", "10 Me Gusta"));
        return pictures;
    }

    public void ShowToolbar(String tittle, boolean upButton, View view)
    {
        //Para el caso de un fragment como este, se usa el view que es donde esta cargado el layaut actual
        //del fragment, y dentro de este view es donde vamos a buscar la toolbar
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        //como trabajamos con soporte debemos aplicar la siguiente linea
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(tittle);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

}
