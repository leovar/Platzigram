package com.platzi.platzigram.views;

import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.view.View;

import com.platzi.platzigram.R;

public class PictureDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_detail);
        ShowToolbar("", true);

        //Para Recivir el efecto de la imagen realizo las siguientes lineas
        // primero valido la version de android con al que estoy trabajando
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getWindow().setStatusBarColor(Color.TRANSPARENT);

            //si quisieramos manipular el efecto como el tiempo etc, se crea un objeto Fade aparte
            //y luego se le asigna al .setExitTransition
            getWindow().setEnterTransition(new Fade());
        }
    }

    public void ShowToolbar(String tittle, boolean upButton)
    {
        //Para el caso de un fragment como este, se usa el view que es donde esta cargado el layaut actual
        //del fragment, y dentro de este view es donde vamos a buscar la toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        //como trabajamos con soporte debemos aplicar la siguiente linea
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(tittle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
    }
}
