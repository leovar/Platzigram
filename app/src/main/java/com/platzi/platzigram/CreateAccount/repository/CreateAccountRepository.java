package com.platzi.platzigram.CreateAccount.repository;

import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.CreateAccount.view.CreateAccountActivity;

/**
 * Created by leonardo.valencia on 23/04/2018.
 */

public interface CreateAccountRepository {

    void JoinUs(String user, String password, CreateAccountActivity createAccountActivity, FirebaseAuth firebaseAuth);

}
