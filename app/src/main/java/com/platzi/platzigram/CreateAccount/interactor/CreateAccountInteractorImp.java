package com.platzi.platzigram.CreateAccount.interactor;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.CreateAccount.presenter.CreateAccountPresenter;
import com.platzi.platzigram.CreateAccount.repository.CreateAccountRepository;
import com.platzi.platzigram.CreateAccount.repository.CreateAccountRepositoryImp;
import com.platzi.platzigram.CreateAccount.view.CreateAccountActivity;

/**
 * Created by leonardo.valencia on 21/04/2018.
 */

public class CreateAccountInteractorImp implements CreateAccountInteractor {

    CreateAccountRepository createAccountRepository;
    CreateAccountPresenter createAccountPresenter;

    public CreateAccountInteractorImp(CreateAccountPresenter createAccountPresenter){
        this.createAccountPresenter = createAccountPresenter;
        createAccountRepository = new CreateAccountRepositoryImp(this);
    }

    @Override
    public void JoinUs(String user, String Password, CreateAccountActivity createAccountActivity, FirebaseAuth firebaseAuth) {
        createAccountRepository.JoinUs(user, Password, createAccountActivity, firebaseAuth);
    }

    @Override
    public void ResultConsultaCrearCuenta(Task<AuthResult> task) {
        if (task.isSuccessful()){
            createAccountPresenter.CuentaCreadaOk("Cuenta creada Exitosamente");
        }else{
            Exception ex = task.getException();
            createAccountPresenter.Error("error " + ex.getMessage());
        }
    }
}
