package com.platzi.platzigram.CreateAccount.view;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.platzi.platzigram.CreateAccount.presenter.CreateAccountPresenter;
import com.platzi.platzigram.CreateAccount.presenter.CreateAccountPresenterImp;
import com.platzi.platzigram.R;

public class CreateAccountActivity extends AppCompatActivity implements CreateAccountView {

    private static final String TAG = "CreateAccountActivity";
    FirebaseAuth firebaseAuth;
    FirebaseAuth.AuthStateListener authStateListener;

    private Button btnjoinUs;
    private TextInputEditText email, password_createaccount, confirmPassword;
    CreateAccountPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        presenter = new CreateAccountPresenterImp(this, this);

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.w(TAG, "Usuario Logueado " + user.getEmail());
                } else {
                    Log.w(TAG, "Usuario no Logueado");
                }
            }
        };

        btnjoinUs = (Button) findViewById(R.id.joinUs);
        email = (TextInputEditText) findViewById(R.id.email);
        password_createaccount = (TextInputEditText) findViewById(R.id.password_createaccount);
        confirmPassword = (TextInputEditText) findViewById(R.id.confirmPassword);
        ShowToolbar(getResources().getString(R.string.toolbar_tittle_createaccount), true);
    }

    public void ShowToolbar(String tittle, boolean upButton) {
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(tittle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(authStateListener);
    }

    /*En este metodo creamos el usuario y validamos su creacion */
    @Override
    public void CrearCuentaLogin(View view) {
        try {
            String user = email.getText().toString();
            String pass = password_createaccount.getText().toString();
            String passConfirm = confirmPassword.getText().toString();

            presenter.JoinUs(user, pass, this, firebaseAuth);

        } catch (Exception e) {
            CreateAccountError(e.getMessage());
        }
    }

    @Override
    public void CreateAccountSuccess(String mensajeOk) {
        Toast.makeText(CreateAccountActivity.this, mensajeOk, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void CreateAccountError(String mensajeError) {
        Toast.makeText(CreateAccountActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
    }
}
