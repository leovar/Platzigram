package com.platzi.platzigram.CreateAccount.presenter;

import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.CreateAccount.interactor.CreateAccountInteractor;
import com.platzi.platzigram.CreateAccount.interactor.CreateAccountInteractorImp;
import com.platzi.platzigram.CreateAccount.view.CreateAccountView;
import com.platzi.platzigram.CreateAccount.view.CreateAccountActivity;

/**
 * Created by leonardo.valencia on 21/04/2018.
 */

public class CreateAccountPresenterImp implements CreateAccountPresenter {

    CreateAccountActivity createAccountActivity;
    CreateAccountInteractor createAccountInteractor;
    private CreateAccountView _createAccountView;

    /*Se crea un constructor de la misma clase que reciba el view para poder trabajár con el contexto
    * donde se necesite*/
    public CreateAccountPresenterImp(CreateAccountActivity createAccountActivity, CreateAccountView createAccountView){
        this.createAccountActivity = createAccountActivity;
        createAccountInteractor = new CreateAccountInteractorImp(this);
        _createAccountView = createAccountView;
    }

    @Override
    public void JoinUs(String user, String password, CreateAccountActivity createAccountActivity, FirebaseAuth firebaseAuth) {
        createAccountInteractor.JoinUs(user, password, createAccountActivity, firebaseAuth);
        //TODO en este metodo le estoy enviando el View al interactor cuando el interactor
        //todo ya se esta instanciando con un View, validar si ni es necesario enviar el View en este metodo
    }

    @Override
    public void Error(String mensajeError) {
        _createAccountView.CreateAccountError(mensajeError);
    }

    @Override
    public void CuentaCreadaOk(String mensajeOk) {
        _createAccountView.CreateAccountSuccess(mensajeOk);
    }
}
