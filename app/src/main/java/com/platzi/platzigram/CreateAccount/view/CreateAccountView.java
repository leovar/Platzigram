package com.platzi.platzigram.CreateAccount.view;

import android.view.View;

/**
 * Created by leonardo.valencia on 26/04/2018.
 */

public interface CreateAccountView {

    void CrearCuentaLogin(View view);

    void CreateAccountError(String mensajeError);
    void CreateAccountSuccess(String mensajeOk);
}
