package com.platzi.platzigram.CreateAccount.interactor;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.CreateAccount.view.CreateAccountActivity;

/**
 * Created by leonardo.valencia on 21/04/2018.
 */

public interface CreateAccountInteractor {

    void JoinUs(String user, String Password, CreateAccountActivity createAccountActivity, FirebaseAuth firebaseAuth);
    void ResultConsultaCrearCuenta(Task<AuthResult> task);

}
