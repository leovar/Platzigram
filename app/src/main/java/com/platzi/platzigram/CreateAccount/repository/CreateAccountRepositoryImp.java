package com.platzi.platzigram.CreateAccount.repository;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.CreateAccount.interactor.CreateAccountInteractor;
import com.platzi.platzigram.CreateAccount.view.CreateAccountActivity;

/**
 * Created by leonardo.valencia on 23/04/2018.
 */

public class CreateAccountRepositoryImp implements CreateAccountRepository {
    CreateAccountInteractor createAccountInteractor;

    public CreateAccountRepositoryImp(CreateAccountInteractor createAccountInteractor) {
        this.createAccountInteractor = createAccountInteractor;
    }

    @Override
    public void JoinUs(String user, String password, CreateAccountActivity createAccountActivityJoin, FirebaseAuth firebaseAuth) {

        /*Creamos el usuario y validamos su creacion con un Listener al crearlo*/
        firebaseAuth.createUserWithEmailAndPassword(user, password)
                .addOnCompleteListener(createAccountActivityJoin, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        createAccountInteractor.ResultConsultaCrearCuenta(task);
                    }
                });
    }
}
