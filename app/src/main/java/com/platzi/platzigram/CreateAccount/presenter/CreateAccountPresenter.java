package com.platzi.platzigram.CreateAccount.presenter;

import com.google.firebase.auth.FirebaseAuth;
import com.platzi.platzigram.CreateAccount.view.CreateAccountActivity;

/**
 * Created by leonardo.valencia on 21/04/2018.
 */

public interface CreateAccountPresenter {

    void JoinUs(String user, String password, CreateAccountActivity createAccountActivity, FirebaseAuth firebaseAuth);

    void Error(String mensajeError);

    void CuentaCreadaOk(String mensajeOk);
}
